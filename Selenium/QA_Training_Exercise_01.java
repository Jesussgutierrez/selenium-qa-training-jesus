package practices;




import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QA_Training_Exercise_01 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		//VARIABLES
		String currentBookName = " ";
		String spectedBookName = "Speaking JavaScript";
		
		String currentUserName = " ";
		String username = "testusername";
		String userpassword = "@Testpassword123";
		
		//------------------------------------------------------------------------------------------------------------------TEST 
		//                                                                                    navigate to the bookstore and select a book and compare the results
		
		/**
		 * configure webdriver and browse url
		 */
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/selenium_Webdriver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/books");
		
		/**
		 * search in a text box
		 */
		driver.findElement(By.id("searchBox")).sendKeys(spectedBookName);
		driver.findElement(By.className("action-buttons")).click();
		
		/**
		 * set a variable with the current name of book
		 */
		currentBookName = driver.findElement(By.cssSelector("div#title-wrapper div:nth-child(2)")).getText();
		
		/**
		 * compare if the names of the books are the same
		 */
		if(currentBookName.contentEquals(spectedBookName)) {
			System.out.println("Test ok - match was found in the searched book");
		}else {
			System.out.println("Test fail - has expected:  "+ spectedBookName +" and got: "+ currentBookName);
			driver.quit();
			Assert.fail();
		}
		Thread.sleep(500);
		//-------------------------------------------------------------------------------------------------------------------TEST 2
		//                                                                                          log in to the page and compare the names of the users
		
		/**
		 * Click in a login button and fill fields
		 */
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("userName")).sendKeys(username);
		driver.findElement(By.id("password")).sendKeys(userpassword);
		driver.findElement(By.id("login")).click();
		
		/**
		 * Waiting for appear the element and set in a currentUserName variable
		 */
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("userName-value")));
		
		currentUserName = driver.findElement(By.id("userName-value")).getText();
		
		
		/**
		 * Compare if the user names are same
		 */
		if(currentUserName.contentEquals(username)) {
			System.out.println("Test 2 ok - match was found in the username");
		}else {
			System.out.println("Test 2 fail - has expected:  "+ username +" and got: "+ currentUserName);
			driver.quit();
			Assert.fail();
		}
		Thread.sleep(300);
		
		
		//-------------------------------------------------------------------------------------------------------------------TEST 3
		//                                                                                add a new tab and open a new window and close and return to the first tab
		
		/**
		 * create a string with the current tab indicator
		 */
		String mainTap = driver.getWindowHandle();
		
		/**
		 * open a new tab and navigate to the new URL
		 */
		driver.switchTo().newWindow(WindowType.TAB);
		driver.get("https://demoqa.com/browser-windows");
		
		/**
		 * click on new window button
		 */
		driver.findElement(By.id("windowButton")).click();
		Thread.sleep(1000);
		
		/**
		 * change control to the new window
		 */
		try {
			for(String winHandle:driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			}
		}catch (Exception e) {
			System.out.println(e);
		}

		/**
		 * maximize and close the new window
		 */
		driver.manage().window().maximize();
		Thread.sleep(1000);
		driver.close();
		Thread.sleep(1000);
		
		/**
		 * return to the first tab
		 */
		driver.switchTo().window(mainTap);
	

		
		Thread.sleep(3000);
		driver.quit();
	
	}
	
}
